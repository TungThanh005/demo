<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Phones extends Model
{
    protected $table='phone';
    protected $fillable=['id','phone_number','user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
