<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table='posts';
    protected $primaryKey = 'id_posts';
    public function tableuser()
    {
        $this->belongsTo(User::class,'id_user','id_posts');
    }
}
