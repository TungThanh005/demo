<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Request;
class textRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return back()->withInput();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'   =>  'required|min:2|max:40|unique:users,username',
            'email'      =>  'required|min:9|max:100|unique:users,email|email',
            'password'   =>  'required|min:2|max:30',
            'avatar'     =>   'image|mimes:jpg,png',
        ];
    }
    public function messages ()
    {
        return [
            'required'   =>  ':attribute không được để trống',
            'min'        =>  ':attribute ít nhất phải có :min kí tự',
            'max'        =>  ':attribute không quá :max kí tự',
            'unique'     =>  ':attribute đã tồn tại',
            'email'     =>  'phải nhập đúng định dạng email',
        ];
    }


}
