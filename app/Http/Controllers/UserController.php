<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Http\Requests\textRequest;




class UserController extends Controller
{
    public function content(){
        return view('admin.layout.content');
    }
    public function trangchu(){
        return view('admin.layout.trangchu');
    }
    public function postLogout()
    {
        Auth::logout();
        return redirect()->route('get_login');
    }
    public function getlogin()
    {
        return view('admin.layout.login');
    }
    public function postlogin(Request $request)
    {
        $data=$request->all();
        $rules=
            [
               'email'      =>  'required|min:9|max:40|email',
               'password'   =>  'required|min:6'
            ];
        $messages=
            [
                'required'  => ':attribute không được để trống',
                'min'       => ':attribute ít nhât là :min kí tự',
                'max'       => ':attribute nhiều nhất là :max kí tự',
                'email'     => 'bạn phải nhập đúng định dang email'

            ];
        $customAttribute=
            [
                'email'     => 'email',
                'password'  => 'mật khẩu'
            ];

         $validation=Validator::make($data,$rules,$messages,$customAttribute);
          if ($validation->fails()){
              return back()->withErrors($validation)->withInput();
          }


          if (Auth::attempt(['email'=>$request->email,'password'=>$request->password]))
          {   if (Auth::user()->quyen==1)
              return redirect()->route('content');
               else
                   return redirect()->route('trangchu');
          }else{
              return redirect()->route('get_login')->with('thongbao','bạn nhập sai tài khoan of mật khẩu');
          }
    }

    /* public function content();
 {
     return view('admin.layout.content');
 }

     public function table()
     {
         return view('admin.pages.table_user');
     }

     public function form()
     {
         return view('admin.pages.form_user');
     }

     public function form_post(Request $request)
     {

         return view('admin.pages.form_post');
     }

     public function table_post(Request $request)
     {
         $data = posts::all();

         return view('admin.pages.table_post', ['name' => $data]);
     }

     public function form_category()
     {
         return view('admin.pages.form_category');
     }

     public function table_category()
     {
         return view('admin.pages.table_category');
     }

     public function test()
     {
         $array = array(1, 2, 3, 4, 5, 6);
         return view('btvn', ['tung1' => $array]);
     }



     public function list_users(Request $request)
     {
         $users = User::all();
         return view('admin.pages.list_users', ['name' => $users]);
     }

     public function form_posts(Request $request)
     {

         $data = ($request->all());
         $posts = new posts();
         $posts->title = $data['title'];
         $posts->description = $data['description'];
         $posts->content = $data['content'];
         $posts->view = $data['view'];
         $posts->user_id = $data['user_id'];
         $posts->category_id = $data['category_id'];
         $posts->save();
         return redirect('/table_post');

     }

     public function edit_post($id)
     {
         $post = posts::where('id_posts', $id)->get()->toArray();
        return view('admin.pages.update_post',['name'=>$post]);
     }
     public function updatepost(Request $request)
     {
 $data=($request->all());
 dd($data);
 /*$flight = posts::find();*/




    public function get_them(){
        return view('admin.users.form_user');
    }
    public function post_them(textRequest $request)
    {
        /*$data = ($request->all());

        $rules=
            [
                'username'   =>  'required|min:2|max:40|unique:users,username',
                'email'      =>  'required|min:9|max:100|unique:users,email|email',
                'password'   =>  'required|min:2|max:30',
                'avatar'     =>   'image|mimes:jpg,png',
            ];

        $messages=
            [
                'required'   =>  ':attribute không được để trống',
                'min'        =>  ':attribute ít nhất phải có :min kí tự',
                'max'        =>  ':attribute không quá :max kí tự',
                'unique'     =>  ':attribute đã tồn tại',
                'email'     =>  'phải nhập đúng định dạng email',
            ];

         $customAttribute=
             [
               'username'    =>  'tên đăng nhập',
               'password'    =>  'mật khẩu'
             ];


        $validate=Validator::make($data,$rules,$messages,$customAttribute);
        if ($validate->fails()){
            return back()->withErrors($validate)->withInput();
        }*/

        if ($request->hasFile('avatar')){
            $file=$request->avatar;
            $name=$file->getClientOriginalName();
            $name1=explode('.',$name);
            $extension=$file->getClientOriginalExtension();
            $path=public_path('img');
            $name2=$name1[0].time().'-'.$extension;
            $file->move($path,$name2);


        }

        $user = new User($request->all());
        $user->username = $request->username;
        $user->phone = $request->phone;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->address = $request->address;
        $user->name = $request->name;
        $user->quyen=0;
        $user->avatar=$name2;
        $user->remember_token=$request->_token;
        $user->save();
        return redirect()->route('danhsach_user')->with('thongbao','bạn đã tạo mới user thành công');

    }



    public function danhsach(){
        $data = User::paginate(1);
        return view('admin.users.table_user', ['paginator' => $data]);
    }

    public function get_sua($id){
        $data=User::find($id);

        return view('admin.users.update_user',['name'=>$data]);

    }
    public function post_sua(Request $request, $id)
    {
        $data=$request->all();


        $rules=
            [
                'username'   =>  'required|min:2|max:40|unique:users,username,' .$id,
                'password'   => 'min:6|max:30',
                'email'      =>  'required|min:9|max:100|email|unique:users,email,' .$id,

            ];

        $messages=
            [
                'required'   =>  ':attribute không được để trống',
                'min'        =>  ':attribute ít nhất phải có :min kí tự',
                'max'        =>  ':attribute không quá :max kí tự',
                'unique'     =>  ':attribute đã tồn tại',
                 'email'     =>  'ban chua nhap dung dinh dang email'
            ];

        $customAttribute=
            [
                'username'    =>  'tên đăng nhập',
                'password'    => 'mat khau'

            ];


        $validate=Validator::make($data,$rules,$messages,$customAttribute);
        if ($validate->fails()){
            return back()->withErrors($validate);
        };


        $user=User::find($id);
        $user->username = $data['username'];
        $user->phone = $data['phone'];
        $user->email = $data['email'];
        if (!is_null( $data['password'])) {
            $user->password = bcrypt($data['password']);
        };
        $user->address = $data['address'];
        $user->name = $data['name'];
        $user->save();
        return redirect()->route('danhsach_user');
    }
    public function xoa(Request $request){
        $id=$request->id;
        $user=User::find($id);
        $user->delete();
        echo "okie";

    }
}
