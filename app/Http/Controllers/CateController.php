<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Category;

class CateController extends Controller
{
    public function get_them(){
        return view('admin.category.form_category');
    }
    public function post_them(Request $request){

        $data = ($request->all());
        $ketqua=[
         'namecategory'=>'required|min:2|max:10|unique:category,name'
        ];
        $thongbao=[
            'required'=>':attribute khong duoc de trong',
            'min'=>':attribute do dai it nhat la 2 ki tu',
            'max'=>':attribute do dai lon  nhat la 10 ki tu',
            'unique'=>':attribute da ton tai'
            ];
        $suadoi=[
            'namecategory'=>'ten'
        ];
        $validate=validator::make($data,$ketqua,$thongbao,$suadoi);
        if ($validate->fails()){
            return back()->withErrors($validate)->withInput();
        }


        $cate = new Category();
        $cate->name = $data['namecategory'];

        $cate->save();
        return redirect()->route('danhsach_cate')->with('thongbao','ban da them thanh cong');
    }


    public function danhsach(){
        $data = Category::all();

        return view('admin.category.table_category',['name'=>$data]);
    }



  public function get_sua($id){
        $data= Category::find($id);

        return view('admin.category.form_sua',['name'=>$data]);
  }
  public function post_sua(Request $request, $id){

        $cate=Category::find($id);
        $this ->validate($request,
      [
            'namecategory'=>'required|min:2|max:10|unique:category,name'
        ],
      [
             'namecategory.required'=>':attribute khong duoc de trong',
             'namecategory.min'=>':attribute toi thieu 2 ki tu',
             'namecategory.max'=>':attribute toi da 10 ki tu',
             'namecategory.unique'=>':attribute ten phai la duy nhat'
         ]);
       /* $cate->name=$request->name;*/

       /* $validate= validator::make($cate,$rules,$messages,$customAttributes);*/
        /* $validate=validator::make($cate,$ketqua,$thongbao,$suadoi);*/


        $cate->name=$request->namecategory;
        $cate->save();
        return redirect()->route('danhsach_cate')->with('baocao','ban da sua thanh cong');
  }
  public function xoa($id){
        $cate=Category::find($id);
        $cate->delete();
        return redirect()->route('danhsach_cate');
  }
}
