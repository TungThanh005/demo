<?php

namespace App\Http\Controllers;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\User;


class PostController extends Controller
{
   public function get_them(){
       $data2=User::all();
       $data1=Category::all();
     return view('admin.posts.form_post',['user'=>$data2, 'cate'=>$data1]);
   }
   public function post_them(Request $request){
       $data = ($request->all());
       $rules=
           [

           ];
       $posts = new Post();
       $posts->title = $data['title'];
       $posts->description = $data['description'];
       $posts->content = $data['content'];
       $posts->view = $data['view'];
       $posts->user_id = $data['user_id'];
       $posts->category_id = $data['category_id'];
       $posts->save();
       return back();

   }


   public function danhsach(){

       $data = Post::paginate(5);
   /* dd($data);*/

       return view('admin.posts.table_post',['name'=>$data],['i'=>0]);
   }

   public function get_sua($id){
       $data2=User::all();
       $data1=Category::all();
       $data=Post::find($id);
       return view('admin.posts.update_post',['name'=>$data,'name1'=>$data1,'user'=>$data2]);
   }
   public function post_sua(Request $request, $id)
   {
       $data=Post::find($id);
       $data->title=$request->title;
        $data->description=$request->description;
        $data->content=$request->content;
        $data->view=$request->view;
        $data->user_id=$request->user_id;
        $data->category_id=$request->category_id;
        $data->save();
        return redirect()->route('danhsach_post');

   }
   public function xoa($id){
       $post=Post::find($id);
       $post->delete();
       return redirect()->route('danhsach_post');
   }

}
