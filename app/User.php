<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Phones;
use App\Role;
class User extends Authenticatable
{

    protected $table='users';
    protected $primaryKey = 'id_user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','username','id_users'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function phoneNumber()
    {
        return $this->hasOne(Phones::class,'user_id','id_user');
    }
    public function tablepost()
    {
     return $this->hasMany(Post::class,'user_id','id_user');
    }
    public function roles(){
        return $this->belongsToMany(Role::class,'role_user','user_id','role_id');

    }
}
