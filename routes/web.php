<?php
/*Route::get('/about/{tung}','homeControllers@new');
Route::get('tung3/{name1}','editConTrollerrs@news');
Route::get('tung1',function (){
    return view('btvn');
});
Route::post('tung2',function (){
    $name=array(
      "tung1"=>"1",
        "tung2"=>"2",
        "tung7"=>"3",
        "tung8"=>"4"
    );
    return view('day4',['names'=>$name]);
})->name('submitform');
Route::get('tung5/{tung}/{tung1}', function ($tung,$tung1){
    echo 'ten cua toi la: '.$tung.'và'.$tung1;
})->where(['tung'=>'[0-9]+','tung1'=>'[a-z]+']);
Route::get('day5-1', function(){
    return view('tung');
});*/

/*tempalde admin*/
Route::get('admin/login','UserController@getlogin')->name('get_login');
Route::post('admin/login','UserController@postlogin')->name('post_login');
Route::get('admin/logout','UserController@logout')->name('logout');
Route::post('admin/logout','UserController@postLogout')->name('postlogout');





Route::group(['prefix'=>'admin'],function (){
    Route::group(['prefix'=>'category'],function (){
        Route::get('danhsach','CateController@danhsach')->name('danhsach_cate');

        Route::get('them','CateController@get_them')->name('get_them_cate');
        Route::post('them','CateController@post_them')->name('post_them_cate');

        Route::get('sua/{id}', 'CateController@get_sua')->name('get_sua_cate');
        Route::post('sua/{id}', 'CateController@post_sua')->name('post_sua_cate');

        Route::get('delate_cate/{id?}','CateController@xoa')->name('delete_cate');

    });
    Route::group(['prefix'=>'posts'],function (){
        Route::get('danhsach','PostController@danhsach')->name("danhsach_post");

        Route::get('them','PostController@get_them')->name('get_them_post');
        Route::post('them','PostController@post_them')->name('post_them_posts');

        Route::get('sua/{id}','PostController@get_sua')->name('get_sua_posts');
        Route::post('sua/{id}','PostController@post_sua')->name('post_sua_posts');

        Route::get('xoa_post/{id?}','PostController@xoa')->name('delete_post');
    });
    Route::group(['prefix'=>'users'],function (){
        Route::get('/danhsach','UserController@danhsach')->name('danhsach_user');

        Route::get('/them','UserController@get_them')->name('get_them_user');
        Route::post('them','UserController@post_them')->name('post_them_user');

        Route::get('sua/{id}','UserController@get_sua')->name('get_sua_user');
        Route::post('sua/{id}','UserController@post_sua')->name('post_sua_user');


        Route::post('delate_user/{id?}','UserController@xoa')->name('delete_user');
    });

});
Route::get('/','UserController@content')->name('content');
Route::get('ajax_post/{id}','AjaxController@get_id');
Route::get('trangchu','UserController@trangchu')->name('trangchu');
Route::get('/home', 'HomeController@index')->name('home');

Route::get('test',function (){
   $t=\Illuminate\Support\Facades\DB::table('users')
       ->whereNull('remember_token')->get();

    dd($t);
});





















//tìm kiếm theo id
Route::get('find',function (){
    $user=\App\User::find(4);
    dd($user);
});
//tìm kiếm theo id nếu ko tì thấy sẽ trả về thông báo lỗi
Route::get('findOrFail',function (){
    $user=\App\User::findOrFail(4);
    dd($user);

});
// chunk lấy từng vùng dữ liệu ra 1 để xử lý
Route::get('lists',function (){
    $user=\App\User::chunk(40, function ($flights) {
        $name='';
        foreach ($flights as $flight) {
           $name.= $flight;
        }
        dd($name);
    });
});

//lấy tất ca dữ liệu
Route::get('all',function (){
    $user=\App\User::all();
    dd($user);
});
//Điều kiện where = , < , > ,>= , =<
Route::get('where',function (){
    $user=\App\User::where('id_user','>',1)->get();
    dd($user);
});
//where in tìm kiến trong model những cột được chỉ định
//where not in tìm kiếm ngoài những cột được chỉ định
Route::get('wherein',function (){
    $user=\App\User::all()->wherein('id_user',array(4, 9,26));
    dd($user);
});
//where null và notnull kiểm tra gia trị của cột có phải null hay ko
Route::get('null',function (){
    $user=\App\User::whereNull('name')->get();
    dd($user);
});
//where date , where month , where day, where year tìm kiếm theo các ngày, tháng , năm
Route::get('wheredate',function (){
    $user=\App\User::whereDate('created_at','2018-08-03')->get();
    dd($user);
});
// lấy theo trường trong database
Route::get('select',function (){
    $user=\App\User::select('name','email')->get();
    dd($user);
});
// lấy gia trị đầu tiên
Route::get('first',function (){
    $user=\App\User::select('name','email')->first();
    dd($user);
});
//toArray chuyển kết quả nhận được sang dạng mảng
Route::get('toArray',function (){
    $user=\App\User::all()->toArray();
    dd($user);
});
//raw
Route::get('raw',function (){
    $user = DB::table('users')
        ->select(DB::raw('count(*) as user_count, id_user'))
        ->groupBy('id_user')
        ->get();
    dd($user);
});

//orderby nhận 2 giá trị mặc định là ASC sắp xếp tăng dần, DESC sắp xếp giảm dần
Route::get('orderby',function (){
    $user=\App\User::where('id_user','>',1) ->orderBy('name','DESC')
        ->get();
    dd($user);
});

// count đến xem trong bảng user có bao nhiêu bản ghi
Route::get('count',function (){
    $user=\App\User::all()->count();
    dd($user);
});
// max tìm giá trị lớn nhất trong cột id_user
Route::get('max',function (){
    $user=\App\User::where('id_user','>',1)->max('id_user');
    dd($user);
});
// min tìm giá trị nhỏ nhất trong cột id_user
Route::get('min',function (){
    $user=\App\User::where('id_user','>',1)->min('id_user');
    dd($user);
});
// like tìm kiếm trong bảng user mà trong trường email có từ 'de' hoặc trong trường name có từ ab
Route::get('like',function (){
    $user=\App\User::where('email','like','%'.'de'.'%')->orwhere('name','like','%'.'ab'.'%')->get();
    dd($user);
});
// limit lấy từ bản ghi số 20 lấy 5 giá trị
Route::get('limit',function (){
    $user=\App\User::skip(20)->take(5)->get();
    dd($user);
});
//insert
Route::get('insert',function (){

    $user=new \App\User();
    $user->username='addnew';
    $user->email='tungthanhnp1111@gmail.com';
    $user->password=bcrypt('123456');
    $user->quyen=0;
    $user->phone='098925169';
    $user->avatar='anh1.jpg';
    $user->address='phú xuyên- hà nội';
    $user->name='tung';
    $user->remember_token='asdfsfsfsfsdffdsfsfdsfsdfsfdsfdsfd';
    $user->save();
});
//update cho id =9
Route::get('update',function () {
    $user=\App\User::find(9);
    $user->username='edit';
    $user->email='edit@gmail.com';
    $user->password=bcrypt('123456');
    $user->save();
});
// delete
Route::get('delete',function () {
    $user=\App\User::find(4);
    $user->delete();
});
//pluck lấy ra tất cả gia trị đầu tiên đưuọc chỉ định
Route::get('pluck',function (){
    $user=\App\User::where('id_user','>',1)->pluck('name');
    dd($user);
});
//value chỉ lấy ra duy nhất 1 giá trị đầu tiên của trường được chỉ định tỏng value
Route::get('value',function (){
    $user=\App\User::where('id_user','>',1)->value('name');
    dd($user);
});
//between lấy theo id_user trong khoảng giá trị từ 4 đến 56 lấy cả gí trị bằng
// not between lấy những bản ghi theo trường id  khoảng nằm ngoài khoảng từ 4->56
Route::get('between',function (){
    $user=\App\User::whereBetween('id_user',[4,56])->get();
    dd($user);
});
//join trong bảng users và bảng phone có quan hệ 1 vs 1 trong function sử dụng quan hệ từ bảng user lấy ra sdt của user đó qua quan hệ phone Number
Route::get('join',function (){
    $users=\App\User::find(4);
    $user=$users->phoneNumber->phone_number;
    dd($user);
});


