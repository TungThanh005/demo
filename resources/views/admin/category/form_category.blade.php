@extends('admin.master')
@section('content')
    <div class="col-lg-6">
        <h1>ADD Category</h1>
        <form action="{!! route('post_them_cate') !!}" method="post" role="form">
            @csrf
            <div class="form-group">
                @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $err)
                            {!! $err !!}
                        @endforeach
                    </div>
                 @endif

                <label for="disabledSelect">Name Category</label>
                <input class="form-control" id="namecategory" value="{!! old('namecategory') !!}" name="namecategory" type="text" placeholder="vui lòng nhập name ">
                    @if($errors->has('namecategory'))
                        <span class="text-danger">{!! $errors->first('namecategory') !!}</span>
                    @endif
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-primary" name="submit_post" value="submit">
            </div>

        </form>
    </div>


@endsection