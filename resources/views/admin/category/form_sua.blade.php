@extends('admin.master')
@section('content')
    <div class="col-lg-6">
        <h1>ADD Category</h1>
        @if($errors->any())
            <div class="alert alert-danger">
                @foreach($errors->all() as $err)
                    {!! $err !!}
                 @endforeach
            </div>
        @endif

        <form action="{!! route('post_sua_cate',['id'=>$name->id_category]) !!}" method="post" role="form">
            @csrf

            <div class="form-group">
                <label for="disabledSelect">Name Category</label>
                <input class="form-control" id="namecategory" " name="namecategory" type="text" value="{!! $name->name !!}" placeholder="vui lòng nhập name ">
                @if($errors->has('namecategory'))
                    <span class="text-danger">{!! $errors->first('namecategory') !!}</span>
                    @endif
            </div>

            <div class="form-group">
                <input type="submit" class="btn btn-primary" name="submit_post" value="submit">
            </div>

        </form>
    </div>


@endsection