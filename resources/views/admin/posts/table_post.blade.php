@extends('admin.master')
@section('content')
    <div class="col-lg-6">

        <div class="panel panel-default">

            <div class="panel-heading h1-1">
                <h1>Danh Sách Post</h1>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>STT</th>
                            <th>Title</th>
                            <th>Desecription</th>
                            <th>Content</th>
                            <th>View</th>
                            <th>User_id</th>
                            <th>Category_id</th>
                            <th>Ngày tạo</th>
                            <th>active</th>
                        </tr>
                        </thead>
                        <tbody>

                       @foreach($name as $key =>$values)

                         <tr>
                            <td>{!! ((($name->currentPage() * 5)-5)+1)+$key !!}</td>
                            <td>{!! $values['title'] !!}</td>
                            <td>{!! $values['description'] !!}</td>
                            <td>{!! $values['content'] !!}</td>
                            <td>{!! $values['view'] !!}</td>
                            <td>{!! $values['user_id'] !!}</td>
                            <td>{!! $values['category_id'] !!}</td>
                            <td>{!! $values['updated_at'] !!}</td>
                            <td>
                                <a href="{!! route('get_sua_posts',['id'=>$values->id_posts]) !!}" CLASS="glyphicon glyphicon-edit"></a>
                                <a href="javascript:void(0)" onclick="delete_post({!! $values->id_posts !!})" CLASS="glyphicon glyphicon-trash"></a>
                            </td>
                        </tr>
                           @endforeach

                        </tbody>
                    </table>
                    {!! $name->links() !!}
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->

        </div>
        </div>
        <!-- /.panel -->
    </div>
@endsection
@section('script')
    <script>
        function delete_post(id) {
           tung=confirm('ban co muon xoa khong');
           if (tung){
               window.location.href="{!! route('delete_post') !!}"+'/'+id;
           }

        }
    </script>
    @endsection