@extends('admin.master')
@section('content')
    <div class="col-lg-6">
        <h1>ADD POST</h1>
        <form method="post" action="{!! route('post_them_posts') !!}" role="form">
            @csrf
            <div class="form-group">
                <label for="disabledSelect">Title</label>
                <input class="form-control" id="title"  name="title" type="text" placeholder="vui lòng nhập title ">
            </div>
            <div class="form-group">
                <label for="disabledSelect">Description</label>
                <input class="form-control" id="description"  name="description" type="text" placeholder="vui lòng nhập Description ">
            </div>
            <div class="form-group">
                <label for="disabledSelect">Content</label>
                <input class="form-control" id="content"  name="content" type="text" placeholder="vui lòng nhập content ">
            </div>
            <div class="form-group">
                <label for="disabledSelect">View</label>
                <input class="form-control" id="view"  name="view" type="text" placeholder="vui lòng nhập view ">
            </div>

            <div class="form-group">
                <label for="disabledSelect">User_id</label>
                <select id="user_id" name="user_id" class="form-control">
                    <option>xin moi chon</option>
                    @foreach($user as $value)
                    <option value="{!! $value->id_user !!}">{!! $value->name !!}</option>
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="disabledSelect">Category_id</label>
                <select id="category_id" name="category_id" class="form-control">
                    <option>xin moi chon</option>
                    @foreach($cate as $value)
                    <option value="{!! $value->id_category !!}">{!! $value ->name !!}</option>
                        @endforeach
                </select>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" name="submit_post" value="submit">
            </div>

        </form>
    </div>
@endsection