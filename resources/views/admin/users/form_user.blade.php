@extends('admin.master')
@section('content')
<div class="col-lg-6">

    <h1>ADD USER</h1>
    @if($errors->any())
        <div class="alert alert-danger">
            @foreach($errors->all() as $err)
                {!! $err !!}<br>
            @endforeach
        </div>
    @endif
    <form action="{!! route('post_them_user') !!}" method="post" role="form" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
                <label for="disabledSelect">User Name</label>
                <input class="form-control" id="username" value="{!! old('username') !!}" name="username" type="text" placeholder="vui lòng nhập user name ">
                @if($errors->has('username'))
                    <span class="text-danger">{!! $errors->first('username') !!}</span>
                @endif
            </div>
            <div class="form-group">
                <label for="disabledSelect">Email</label>
                <input class="form-control" id="email" value="{!! old('email') !!}" name="email" type="text" placeholder="vui lòng nhập mail ">
            </div>
                @if($errors->has('email'))
                    <div class="text-danger">{!! $errors->first('email') !!}</div>
                @endif
            <div class="form-group">
                <label for="disabledSelect">Password</label>
                <input class="form-control" id="password"  name="password" type="password" placeholder="vui lòng nhập mật khẩu ">
                <img src="{!! asset('image/mui.png') !!}" alt="">
            </div>
             @if($errors->has('password'))
                 <div class="text-danger">{!! $errors->first('password') !!}</div>
             @endif

            <div class="form-group">
                <label for="disabledSelect">phone</label>
                <input class="form-control" id="phone"  value="{!! old('phone') !!}" name="phone" type="text" placeholder="vui lòng nhập SDT ">
            </div>
            <div class="form-group">
                <label for="disabledSelect">address</label>
                <input class="form-control" id="address" value="{!! old('address') !!}" name="address" type="text" placeholder="vui lòng nhập địa chỉ ">
            </div>
            <div class="form-group">
                <label for="disabledSelect">Name</label>
                <input class="form-control" id="name" value="{!! old('name') !!}" name="name" type="text" placeholder="vui lòng nhập tên của bạn ">
            </div>

        <div class="form-group">
            <label for="disabledSelect">avatar</label>
            <input class="form-control" id="avatar" name="avatar" type="file" placeholder="vui lòng nhập tên của bạn " value="{!! old('avatar') !!}">
        </div>

        <div class="form-group">
            <input type="submit" class="btn btn-primary" name="submit_user" value="submit">
        </div>
    </form>
</div>
@endsection