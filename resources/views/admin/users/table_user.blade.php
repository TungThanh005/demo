@extends('admin.master')
@section('content')
<div class="col-lg-12">

    @if(Session::has('thongbao'))
        <div class="alert alert-success">
            {!! Session::get('thongbao') !!}
        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading h1-1">
           <h1>Danh Sách User</h1>
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>User Name</th>
                        <th>Email</th>
                        <th>phone</th>
                        <th>address</th>
                        <th>Name</th>
                        <th>Ngày tạo</th>
                        <th>Ngày sủa</th>
                        <th>active</th>
                        <th>chức vụ</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($paginator as $key => $value)

                    <tr>
                        <td>{!! $key+1 !!}</td>
                        <td>{!! $value->username !!}</td>
                        <td>{!! $value-> email!!}</td>
                        <td>{!! !is_null($value->phoneNumber)? $value->phoneNumber->phone_number : '' !!}</td>
                        <td>{!! $value-> address!!}</td>
                        <td>{!! $value->name !!}</td>
                        <td>{!! $value-> created_at!!}</td>
                        <td>{!! $value-> updated_at!!}</td>

                        <td>
                            <a href="{!! route('get_sua_user',['id'=>$value->id_user]) !!}" CLASS="glyphicon glyphicon-edit"></a>
                            <a href="javascript:void(0)" onclick="delete_user({!! $value->id_user !!})" CLASS="glyphicon glyphicon-trash"></a>
                        </td>
                        <td>
                            @if(!is_null($value->roles))
                                @foreach($value->roles as $role)
                                    {!! $role->name !!}
                                @endforeach
                            @endif
                        </td>

                    </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
            {{ $paginator->links('vendor.pagination.simple-bootstrap-4') }}
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
@endsection
@section('script')
    <script>

            function delete_user(id) {
                check=confirm('ban co chac la se xoa khong?');
                if (check){
                   $.ajax({
                        'url':'{!! route('delete_user') !!}',
                         'type':'post',
                           'datatype':{'id':id},
                          success:function (data) {
                              alert(data);
                          }
                   });
                }
            }

    </script>

    
    @endsection
